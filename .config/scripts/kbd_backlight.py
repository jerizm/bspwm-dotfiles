#!/usr/bin/env python

import re
import subprocess
import optparse

"""
kbd_backlight.py --write 1 -> write last keyboard backlight value
kbd_backlight.py --restore 1 -> restore last keyboard backlight value
saves value to plain text file
"""

KBD_OUTPUT_PATH = '/home/benjamin/.cache/kbd_bl_value.txt'


def main():
    current_brightness = get_current_brightness()
    options = get_arguments()
    if options.restore:
        backlight_value = read_saved_backlight_value()
        kbd_command(backlight_value)
    else:
        write_new_backlight_value(current_brightness)


def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option('--restore', dest='restore', help='restore 1 to restore last brightness level')
    parser.add_option('--write', dest='write', help='write 1 to write current brightness level')
    (options, arguments) = parser.parse_args()
    return options


def get_current_brightness():
    current_brightness_raw = subprocess.check_output(['kbdlight', 'get']).decode()
    current_brightness_int = re.search(r'\d*', current_brightness_raw)
    current_brightness = int(current_brightness_int.group(0))
    return current_brightness


def read_saved_backlight_value():
    with open(KBD_OUTPUT_PATH, 'r') as fr:
        for line in fr:
            backlight_value = line.rstrip('\n')
    return backlight_value


def write_new_backlight_value(new_value):
    new_value = str(new_value)
    with open(KBD_OUTPUT_PATH, 'w') as fw:
        fw.write(new_value)


def kbd_command(backlight_value):
    subprocess.run(['kbdlight', 'set', backlight_value])


main()
